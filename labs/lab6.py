import math
import matplotlib.pyplot as plt


def mass_printer(mass):
    for element in mass:
        print('{:.3f}'.format(element), end=' ')


def mass_cleaner(mass):  # Избавление массива от None
    result = []
    for i in mass:
        if i is None:
            continue
        else:
            result.append(i)
    if len(result) > 0:
        return result
    else:
        return 0


def plot_data(x_arg, y_arg, legend='f(x)', title='Plot f(x)', x_label='x', y_label='f(x)'):
    plt.plot(x_arg, y_arg, 'ko-')
    plt.legend([legend])
    plt.title(title)
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.savefig('klk')


def mass_to_string(mass):
    string = ''
    for element in mass:
        string = string + str(element) + ' '
    return string


def search(mass):
    to_search = input('\nВведите шаблон для поиска совпадений, числа разделяйте 1-им пробелом\n')
    in_search = mass_to_string(mass)
    print('Колличество вхождений шаблона: ', in_search.count(to_search))


def maks_min_printer(mass, func_name):
    mass = mass_cleaner(mass)
    if mass != 0:
        print(f'\n-----\n{func_name} результаты:')
        print('\nМаксимальное значение функции {:}: {:.3f}'.format(func_name, max(mass)))
        print('Минимальное значение функции {:}: {:.3f}'.format(func_name, min(mass)))
        print('\n', mass_to_string(mass))
        search(mass)
    else:
        print(f'\n-----\n{func_name} решений нет')


while True:
    X_mass = []
    a, x_l, x_u, fcn_num, x_method = 0, 0, 0, 0, 0
    struct = [[], [], []]  # 0 - G || 1 - F || 2 - Y
    try:
        a = float(input('Введите "a": '))
        x_l, x_u = float(input('Нижняя граница "x": ')), float(input('Верхняя граница "x": '))
        x_method = int(input('[1 -   Вы введёте шаги | 2 - Вы введёте шаг]: '))
    except ValueError:
        print('Enter number, not letter')
        exit(1)

    x_steps, x_step, x = 0, 0, x_l
    x_old = x
    try:
        if x_method == 1:
            x_steps = int(input('Введите ЦЕЛОЕ число шагов: '))
            x_step = (x_u - x_l) / x_steps
        elif x_method == 2:
            x_step = float(input('Введите значение шага: '))
    except ValueError:
        print('Enter number, not letter')
        exit(1)

    #  Старт вычислений G
    x = x_old
    while x <= x_u:
        if not math.isclose((10 * a ** 2 - 17 * a * x + 6 * x ** 2), 0, abs_tol=10 ** -1):
            G = (9 * (20 * a ** 2 - 31 * a * x + 12 * x ** 2) / (10 * a ** 2 - 17 * a * x + 6 * x ** 2))
            struct[0].append(G)
            X_mass.append(x)
        else:
            struct[0].append(None)
            X_mass.append(x)
        x += x_step

    #  Старт вычислений F
    X_mass = []
    x = x_old
    while x <= x_u:
        F = (-math.atan((7 * a ** 2 - 2 * a * x - 9 * x ** 2)))
        X_mass.append(x)
        struct[1].append(F)
        x += x_step

    #  Старт вычислений Y
    X_mass = []
    x = x_old
    while x <= x_u:
        try:
            Y = (-math.atan((2 * a ** 2 + a * x - 3 * x ** 2)))
            X_mass.append(x)
            struct[2].append(Y)
        except ValueError:
            X_mass.append(x)
            struct[2].append(None)
        x += x_step

    plot_data(X_mass, struct[0], 'G(x)', 'Plot G(x)', 'x', 'G(x)')
    maks_min_printer(struct[0], 'G')
    plot_data(X_mass, struct[1], 'F(x)', 'Plot F(x)', 'x', 'F(x)')
    maks_min_printer(struct[1], 'F')
    plot_data(X_mass, struct[2], 'Y(x)', 'Plot Y(x)', 'x', 'Y(x)')
    maks_min_printer(struct[2], 'Y')
    while True:
        try:
            a = int(input("\nХотите повторить вычисления снова? 1 - Да: "))
            break
        except ValueError:
            print('Ввод не распознан. Повторите еще раз.')
        continue

    if a != 1:
        print('Have a nice day! :)')
        break
