import math
import matplotlib.pyplot as plt


def mass_printer(mass):
    for element in mass:
        print('{:.3f}'.format(element), end=' ')


def mass_cleaner(mass):  # Избавление массива от None
    result = []
    for i in mass:
        if i is None:
            continue
        else:
            result.append(i)
    if len(result) > 0:
        return result
    else:
        return 0


def plot_data(x_arg, y_arg, legend='f(x)', title='Plot f(x)', x_label='x', y_label='f(x)'):
    plt.plot(x_arg, y_arg, 'ko-')
    plt.legend([legend])
    plt.title(title)
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.show()


def mass_to_string(mass):
    string = ''
    for element in mass:
        string = string + str(element) + ' '
    return string


def search(mass):
    to_search = input('\nВведите шаблон для поиска совпадений, числа разделяйте 1-им пробелом\n')
    in_search = mass_to_string(mass)
    print('Колличество вхождений шаблона: ', in_search.count(to_search))


while True:
    X_mass, G_mass, F_mass, Y_mass = [], [], [], []
    a, x_l, x_u, fcn_num, x_method = 0, 0, 0, 0, 0

    try:
        a = float(input('Введите "a": '))
        x_l, x_u = float(input('Нижняя граница "x": ')), float(input('Верхняя граница "x": '))
        fcn_num = int(input('Введите номер функции [1-G/2-F/3-Y]: '))
        x_method = int(input('[1 -   Вы введёте шаги | 2 - Вы введёте шаг]: '))
    except ValueError:
        print('Enter number, not letter')
        exit(1)

    x_steps, x_step, x = 0, 0, x_l

    try:
        if x_method == 1:
            x_steps = int(input('Введите ЦЕЛОЕ число шагов: '))
            x_step = (x_u - x_l) / x_steps
        elif x_method == 2:
            x_step = float(input('Введите значение шага: '))
    except ValueError:
        print('Enter number, not letter')
        exit(1)

    if fcn_num == 1:
        while x <= x_u:
            if not math.isclose((10 * a ** 2 - 17 * a * x + 6 * x ** 2), 0, abs_tol=10**-1):
                G = (9 * (20 * a ** 2 - 31 * a * x + 12 * x ** 2) / (10 * a ** 2 - 17 * a * x + 6 * x ** 2))
                print('---\nG: {:.3f} || X: {:.3f} |'.format(G, x))
                G_mass.append(G)
                X_mass.append(x)
            else:
                G_mass.append(None)
                X_mass.append(x)
                print('---\n ERROR ZERO DIVISION\nG: NO || X: {:.3f} |'.format(x))
            x += x_step
        plot_data(X_mass, G_mass, 'G(x)', 'Plot G(x)', 'x', 'G(x)')

        G_mass = mass_cleaner(G_mass)  # Очищаю текущий массив
        if G_mass != 0:
            print('\nРезультаты:')
            print('\nМаксимальное значение функции G: {:.3f}'.format(max(G_mass)))
            print('Минимальное значение функции G: {:.3f}'.format(min(G_mass)))
            print('\n', mass_to_string(G_mass))
            search(G_mass)
        else:
            print('\nРешений нет')

    elif fcn_num == 2:
        while x <= x_u:
            F = (-math.atan((7 * a ** 2 - 2 * a * x - 9 * x ** 2)))
            print('---\nF: {:.3f} || X: {:.3f} |'.format(F, x))
            X_mass.append(x)
            F_mass.append(F)
            x += x_step
        plot_data(X_mass, F_mass, 'F(x)', 'Plot F(x)', 'x', 'F(x)')

        F_mass = mass_cleaner(F_mass)  # Очищаю текущий массив
        if F_mass != 0:
            print('\nРезультаты:')
            print('\nМаксимальное значение функции F: {:.3f}'.format(max(F_mass)))
            print('Минимальное значение функции F: {:.3f}'.format(min(F_mass)))
            print('\n', mass_to_string(F_mass))
            search(F_mass)
        else:
            print('\nРешений нет')

    elif fcn_num == 3:
        while x <= x_u:
            Y = (-math.atan((2 * a ** 2 + a * x - 3 * x ** 2)))
            print('---\nY: {:.3f} || X: {:.3f} |'.format(Y, x))
            print('---\nY: {:.3f} || X: {:.3f} |'.format(Y, x))
            X_mass.append(x)
            Y_mass.append(Y)
            x += x_step
        plot_data(X_mass, Y_mass, 'Y(x)', 'Plot Y(x)', 'x', 'Y(x)')  # Рисую график

        Y_mass = mass_cleaner(Y_mass)  # Очищаю текущий массив
        if Y_mass != 0:
            print('\nРезультаты:')
            print('\nМаксимальное значение функции Y: {:.3f}'.format(max(Y_mass)))
            print('Минимальное значение функции Y: {:.3f}'.format(min(Y_mass)))
            print('\n', mass_to_string(Y_mass))
            search(Y_mass)
        else:
            print('\nРешений нет')
    else:
        print('ERROR: номер операции не равен 1/2/3')

    while True:
        try:
            a = int(input("\nХотите повторить вычисления снова? 1 - Да: "))
            break
        except ValueError:
            print('Ввод не распознан. Повторите еще раз.')
        continue

    if a != 1:
        print('Have a nice day! :)')
        break
