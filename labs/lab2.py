import math
import matplotlib.pyplot as plt
while True:
    try:
        while True:
            b = int(input('Выбирайте уравнение {1-G, 2-F, 3-Y}: '))
            if b < 1 or b > 3:
                print('Нет такого уравнения')
            else:
                break
        x = float(input('Введите x: '))
        a = float(input('Введите a: '))
        break
    except ValueError:
        print ('Пожалуйста пишите цифры')

if b == 1:
    g1 = (9 * (20 * a ** 2 - 31 * a * x + 12 * x ** 2))
    g2 = (10 * a ** 2 - 17 * a * x + 6 * x ** 2)
    if math.isclose(g2, 0, abs_tol=10e-6):
        print('Error: На ноль делить нельзя')
    else:
        g = g1 / g2
        print('G = {}'.format(g))
elif b == 2:
    f1 = (7 * a ** 2 - 2 * a * x - 9 * x ** 2)
    f = (-math.atan(f1))
    print('F = {}'.format(f))
elif b == 3:
    y1 = (2 * a ** 2 + a * x - 3 * x ** 2)
    y = (-math.atan(y1))
    print('Y = {}'.format(y))
