number = input('Введите число: ')
if number.count('.') != 0 or number.count(',') != 0:
    print('Число не является целым')
else:
    count = 0
    for i in number:
        if int(i) % 2 == 0:
            count += 1
    print('Колличество чётных цифр в числе: ', count)
